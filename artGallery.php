<?php
include('./php/include/training_header.php');
include('./php/include/navbar4.php');

?>

<link rel="stylesheet" href="assests/css/artGallery.css">

<div class="container">
    <!-- <h1 class="title">Iscool Art Gallery</h1> -->
    <div class="image-container">
        <img src="assests/images/pic-1.jpg" alt="">
        <img src="assests/images/pic-2.jpg" alt="">
        <img src="assests/images/pic-3.jpg" alt="">
        <img src="assests/images/pic-4.jpg" alt="">
        <img src="assests/images/pic-5.jpg" alt="">
        <img src="assests/images/pic-6.jpg" alt="">
        <img src="assests/images/pic-7.jpg" alt="">
        <img src="assests/images/pic-8.jpg" alt="">
    </div>
</div>
<!-- </div> -->


<!-- ===================== footer ===================== -->
<?php
    include('./php/include/footer_section.php');
    include('./php/include/footer.php');
?>
